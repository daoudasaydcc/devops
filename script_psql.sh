#!/bin/bash


# Fonction pour install  postgresql
install_postgresql(){
	sudo apt-get update
	sudo apt upgrade
	sudo apt-get install -y postgresql postgresql-contrib 
	
	# se connecter avec l'utilisateur postgres
	sudo su - postgres   <<finpos 

	# connection a la base de donne postgres avec l'utilisateur postgres
	psql -U postgres -d postgres <<finpsl

	-- creation d'utilisateur
	create user prt WITH ENCRYPTED PASSWORD 'prt' LOGIN SUPERUSER;

	psql -tAc "SELECT 1 FROM pg_roles WHERE rolname = 'prt'" | grep -q 1
	if [ $? -eq 0 ]; then
    echo "L'utilisateur 'prt' existe déjà."


	-- creation du database bdprt
	create database bdprt;
	create database bbofi;
	-- connexion a la base bdprt
	\c bdprt
 
	-- creation du schema
	create schema EXP;
	set search_path=exp;
	-- creation de 2 tables dans le schema EXP
	create table EXP.facture(id int, prix numeric);
	create table EXP.client(id int, nom varchar(255), prenom varchar(255), ville varchar(255));

	-- insertion des donnees
	INSERT INTO EXP.facture (id,prix) VALUES (1,15), (2,20), (3,37);
	INSERT INTO EXP.client (id,nom, prenom, ville) VALUES (1,'Daouda', 'Saidi', 'Montpellier'), (2, 'Kadri', 'Habibu', 'Hanoi'), (3 ,'Aziz', 'Konate', 'Marseille');

finpsl
finpos
}

exportation_schema(){

	sudo su - postgres <<mk
	mkdir backup
	pg_dump -U postgres  -F p -n exp -f ./backup/prt_back2.sql bdprt
mk

}

importation_schema(){
	sudo su - postgres  <<imp

#	psql -U postgres -d postgres <<op
	
#	create database bdofi;

#op

	
	-- importation 
	psql -U postgres -d bdofi < ./backup/prt_back2.sql
imp

}



if [ $# -ne 1 ];then
        echo "Nombres de parametres est incorrect"
        exit 1
fi


choix_fonction="$1" 


if [ $choix_fonction = "install_postgresql" ];then
       install_postgresql 
	   
elif [ $choix_fonction = "exportation_schema" ];then
        exportation_schema

elif [ $choix_fonction = "importation_schema" ];then
        importation_schema

elif [ $choix_fonction = "all"]; them
		install_postgresql
		exportation_schema
		importation_schema
else
        "VEUILLEZ ENTREE  install_postgresql,exportation_schema,importation_schema"
        exit 1
fi


